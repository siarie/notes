#!/bin/sh

GIT_URL="https://codeberg.org/siarie/notes/commit"
FMT="<li><a href=\"$GIT_URL/%H\">%h</a> %s</li>"
LOGS=$(git log --format="$FMT" $1)


echo "<details id=\"changelog\">"
echo "<summary>Changelog</summary>"
echo "<ul>"
echo $LOGS
echo "</ul>"
echo "</details>"
