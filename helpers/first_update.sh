#!/bin/sh

TARGET_FILE=$1
OUTPUT=$(git log --reverse --format="%ad" --date="format:%a, %d %b %Y %T %z" -- "$TARGET_FILE" | head -1)

[ -z "$OUTPUT" ] && OUTPUT=$(date -R)
echo "<strong>Publish Date</strong>: <time id="pubdate" datetime=\"$OUTPUT\">$OUTPUT</time><br>"
