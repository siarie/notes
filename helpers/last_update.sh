#!/bin/sh

TARGET_FILE=$1
OUTPUT=$(git log -1 --format="%ad" --date="format:%a, %d %b %Y %T %z" -- "$TARGET_FILE")

[ -z "$OUTPUT" ] && OUTPUT=$(date -R)
echo "<strong>Last Update</strong>: <time id="update" datetime=\"$OUTPUT\">$OUTPUT</time><br>"
